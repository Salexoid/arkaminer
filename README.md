# Space Arkanoid (development name)

Miro board: https://miro.com/app/board/uXjVNXwDuGY=/

Team:
- Juliya Zilya (Game Designer & PM)
- Liudmila Fridman (Artist)
- Aleksandr Sesorov (Programmer & QA)
